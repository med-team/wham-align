#ifndef _ERROR_H_
#define _ERROR_H_

/**
 *    WHAM - high-throughput sequence aligner
 *    Copyright (C) 2011  WHAM Group, University of Wisconsin
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*	$Id: error.h 157 2012-07-25 05:58:09Z yinan $ */

#define SUCCESS			0
#define ERR_FILE		500
#define ERR_PARA		501
#define ERR_MEM			502
#define ERR_CHECK		503
#define ERR_SEQ			504
#define ERR_LONGSEQ		505
#define ERR_READ_FORMAT		506
#define ERR_INDEX		507

#define MSG_HITSETFULL		600

/*
 * macro for message level.
 */
#define ERROR	0
#define WARNING 1
#define INFO	2
#define DEBUG1	3
#define DEBUG2	4
#define DEBUG3	5

void elog(int level, char * format, ...);

#endif


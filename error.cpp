/**
 *    WHAM - high-throughput sequence aligner
 *    Copyright (C) 2011  WHAM Group, University of Wisconsin
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*	$Id: error.cpp 157 2012-07-25 05:58:09Z yinan $ */

#include "error.h"
#include <stdio.h>
#include <stdarg.h>

//int ELOG_LEVEL = DEBUG1;
int ELOG_LEVEL = INFO;

void elog(int level, char * format, ...)
{
  va_list args;
  char *filename = __FILE__;
  int lineno = __LINE__;

  if (level <= ELOG_LEVEL)
  {
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
  }
}

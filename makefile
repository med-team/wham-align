CC=g++
CFLAGS=-c -O3 -Wno-write-strings
#CFLAGS=-c -g -Wno-write-strings
LDLIBS=-lpthread

#CFLAGS+=-DPOPCNT
#CFLAGS+=-DPERFCOUNT
#CFLAGS+=-DDEBUG_STAT
#CFLAGS+=-DDEBUG_HASH_PRINT

#CFLAGS+=-DPAIRALIGN_NO_FILTER
#CFLAGS+=-DPAIRALIGN_BASIC
#CFLAGS+=-DPAIRALIGN_NW
#CFLAGS+=-DPAIRALIGN_BITVECTOR

all: wham wham-build 

wham: makefile hash.o embedhash.o main.o sequence.o aligner.o interval.o edit_distance.o error.o short.o hitset.o perfcounters.o model.o writer.o
	$(CC) $(LDLIBS) -o wham sequence.o hash.o embedhash.o main.o aligner.o interval.o edit_distance.o error.o short.o hitset.o model.o perfcounters.o writer.o

wham-build: makefile hash.o embedhash.o builder.o sequence.o aligner.o interval.o edit_distance.o error.o short.o hitset.o model.o writer.o
	$(CC) $(LDLIBS) -o wham-build sequence.o hash.o embedhash.o builder.o aligner.o interval.o edit_distance.o error.o short.o hitset.o model.o perfcounters.o writer.o

wham-test: makefile unittest.o hash.o embedhash.o sequence.o aligner.o interval.o edit_distance.o error.o short.o hitset.o model.o
	$(CC) $(LDLIBS) -o wham-test unittest.o sequence.o hash.o embedhash.o aligner.o interval.o edit_distance.o error.o short.o hitset.o model.o perfcounters.o

unittest.o: makefile unittest.cpp bitread.h
	$(CC) $(CFLAGS) unittest.cpp

interval.o: makefile interval.cpp interval.h lib.h error.h
	$(CC) $(CFLAGS) interval.cpp

edit_distance.o: makefile edit_distance.cpp edit_distance.h error.h
	$(CC) $(CFLAGS) edit_distance.cpp

embedhash.o: makefile embedhash.cpp embedhash.h hash.h edit_distance.h sequence.h lib.h error.h hitset.h pair.h
	$(CC) $(CFLAGS) embedhash.cpp
	
hash.o: makefile hash.cpp hash.h edit_distance.h sequence.h lib.h error.h hitset.h pair.h
	$(CC) $(CFLAGS) hash.cpp

aligner.o: makefile aligner.cpp aligner.h hash.h sequence.h lib.h error.h short.h hitset.h
	$(CC) $(CFLAGS) aligner.cpp

main.o: makefile main.cpp hash.h aligner.h sequence.h lib.h error.h perfcounters.h rdtsc.h pair.h
	$(CC) $(CFLAGS) main.cpp

sequence.o: makefile sequence.cpp sequence.h lib.h error.h
	$(CC) $(CFLAGS) sequence.cpp

error.o: makefile error.h
	$(CC) $(CFLAGS) error.cpp

short.o: makefile short.cpp short.h sequence.h lib.h error.h
	$(CC) $(CFLAGS) short.cpp

hitset.o: makefile hitset.cpp hitset.h lib.h short.h
	$(CC) $(CFLAGS) hitset.cpp

model.o: makefile model.cpp model.h
	$(CC) $(CFLAGS) model.cpp

writer.o: makefile writer.cpp writer.h sequence.h short.h
	$(CC) $(CFLAGS) writer.cpp

builder.o: makefile builder.cpp hash.h aligner.h sequence.h lib.h error.h short.h
	$(CC) $(CFLAGS) builder.cpp

filter.o: makefile filter.cpp hash.h aligner.h sequence.h lib.h error.h short.h
	$(CC) $(CFLAGS) filter.cpp

sorter.o: makefile sorter.cpp aligner.h
	$(CC) $(CFLAGS) sorter.cpp

perfcounters.o: makefile perfcounters.cpp perfcounters.h
	$(CC) $(CFLAGS) perfcounters.cpp

clean:
	rm wham wham-build *.o

#ifndef _MODEL_H_
#define _MODEL_H_

/**
 *    WHAM - high-throughput sequence aligner
 *    Copyright (C) 2011  WHAM Group, University of Wisconsin
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*	$Id: model.h 157 2012-07-25 05:58:09Z yinan $ */

class AlignerModel {
public:
  static int estimateNumPartition(unsigned int nEntry, int length, int nError,
      bool memory);
  static unsigned int getNumHashtableFitMemory(unsigned int nEntry, int length,
      int nMismatch, int nPartition);
  static unsigned int estimateIndexSpace(unsigned int nEntry, int length,
      int nError, int nPartition);
  static bool isFitMemory(unsigned int nEntry, int length, int nError,
      int nPartitiion);
private:
  static int computeNumLookup(int nError, int nPartition);
  static int computeNumIndex(int nError, int nPartition);
  static unsigned int getFreeMemory();
  static unsigned int estimateHashtableSpace(unsigned int nEntry, int length,
      int nError, int nPartition);
};

#endif
